from flask import Flask, render_template, request
from Processing import get_message
app = Flask(__name__)

@app.route('/', methods=['get', 'post'])
def main():
    message = ''
    if request.method == 'POST':
        IW = float(request.form.get('Величина сварочного тока (IW)'))
        IF = float(request.form.get('Tок фокусировки электронного пучка (IF)'))
        VW = float(request.form.get('Cкорость сварки (VW)'))
        FP = float(request.form.get('Pасстояние от поверхности образцов до электронно-оптической системы (FP)'))

        weld = [[IW, IF, VW, FP]]
        message = get_message(weld)

    return render_template('index.html', message=message)

@app.route('/text')  # http://127.0.0.1:5000 + '/text' = http://127.0.0.1:5000/text
def text():
    return 'Text'

app.run()


